/*
*    Jonathan Ness
*
*    Lots of Google Maps code based on the tutorial by Raja.D.Singh
*    http://www.codeproject.com/Articles/665527/A-GPS-Location-Plotting-Android-Application
*
*/



package com.example.jonathan.assignment1;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.Calendar;


public class MainWindow extends FragmentActivity
                        implements GooglePlayServicesClient.ConnectionCallbacks,
                        com.google.android.gms.location.LocationListener,
                        GooglePlayServicesClient.OnConnectionFailedListener{

    private Location myLocation;        //Current location, updated on locationupdate
    private GoogleMap myMap;            // map reference
    private LocationClient myLocationClient;
    private static final LocationRequest myRequest = LocationRequest.create()
            .setInterval(5000)         // 5 seconds
            .setFastestInterval(16)    // 16ms = 60fps
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    private String day = "http://www.baroto.com.tr/wp-content/uploads/2013/06/sun-3-256.png";
    private String night = "http://www.icon2s.com/wp-content/uploads/2013/07/black-white-metro-moon-icon.png";
    private String img;
    //On creation
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Get a map view.
        getMap();

        //Read the latest addition to the database and put it as name
        DatabaseHandler db = new DatabaseHandler(this);
        int count = db.getLocationCount();
        if (count > 0) {
            UserLocation userLocation = db.getLocation(count);

            TextView welcomeName = (TextView) findViewById(R.id.welcomeNameInput);
            welcomeName.setText(userLocation.getName());
        }




        //Probably fit to put in a separate function.

        // Load a sun or moon depending on time of day
        if(Calendar.getInstance().get(Calendar.HOUR_OF_DAY) > 12){
            img = day;
        } else {
            img = night;
        }

        //Loads the image
        final ImageView imageView = (ImageView) findViewById(R.id.dayNightImage);
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        imageLoader.get(img, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Volley", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    imageView.setImageBitmap(response.getBitmap());
                }
            }
        });

    }


    //After system have been paused, restart map and locationClient
    @Override
    protected  void onResume(){
        super.onResume();

        getMap();
        wakeLocationClient();
    }

    //On pause, stop GPS from tracking position to save power.
    @Override
    public void onPause(){
        super.onPause();
        if(myLocationClient != null){
            myLocationClient.disconnect();
        }
    }


    //Wakes and connects the locationClient
    private void wakeLocationClient() {
        if(myLocationClient == null){
            myLocationClient = new LocationClient(getApplicationContext(),
                    this,       // Connection Callbacks
                    this);      // OnConnectionFailedListener
        }

        myLocationClient.connect();
    }


    //Set up the map fragment.
    private void getMap() {
        if(myMap == null){
            myMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
        }
        if(myMap != null){
            myMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    //LocationClient is connected
    @Override
    public void onConnected(Bundle bundle) {
        myLocationClient.requestLocationUpdates(
                myRequest,
                this); // LocationListener

    }


    //Location Client is disconnected
    @Override
    public void onDisconnected() {
        Toast.makeText(this, "GPS was disconnected", Toast.LENGTH_SHORT).show();
    }


    //On location change, save new location
    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult){
        Toast.makeText(this, "GPS failed to connect", Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Without GPS, the app won't work...", Toast.LENGTH_LONG).show();
    }



    //Button for moving the camera on the map so that you can view your position.
    public void LoadGPSButtonClicked(View view) {
        try {
            myMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                    .target(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()))
                    .zoom(25.5f)
                    .bearing(0)
                    .tilt(25)
                    .build()));
        } catch (Exception e){

            //For when there is no valid GPS coordinates to use
            Toast.makeText(this, "No Coordinates found yet", Toast.LENGTH_SHORT).show();

        }

    }

    //Will gather the GPS data and launch the next activity.
    public void ViewGPSData(View view) {

        Intent logDataIntent = new Intent(this,
                DataLogger.class);

        EditText userNameField = (EditText) findViewById(R.id.welcomeNameInput);
        String userNameExtra = String.valueOf(userNameField.getText());

        //Putting the extra data. IN the case where no GPS is found, put 0
        logDataIntent.putExtra("name", userNameExtra);
        if(myLocation != null) {
            logDataIntent.putExtra("lat", myLocation.getLatitude());
            logDataIntent.putExtra("lon", myLocation.getLongitude());
            logDataIntent.putExtra("alt", myLocation.getAltitude());
        } else {
            logDataIntent.putExtra("lat", 0);
            logDataIntent.putExtra("lon", 0);
            logDataIntent.putExtra("alt", 0);
        }
        startActivity(logDataIntent);

    }
}
