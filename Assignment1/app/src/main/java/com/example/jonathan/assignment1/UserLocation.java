/**
 *      Code based on tutorial by Ravi Tamada
 *      http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/
 */


package com.example.jonathan.assignment1;

import android.location.Location;

public class UserLocation {

    private int ID;
    private String name;
    private double lat;
    private double lon;
    private double altitude;


    public UserLocation(){

    }

    public UserLocation(String name, Location location){
        this.ID = 0;
        this.name = name;
        this.lat = location.getLatitude();
        this.lon = location.getLongitude();
        this.altitude = location.getAltitude();
    }

    public UserLocation(int ID, String name, double lat, double lon, double alt){
        this.ID = ID;
        this.name = name;
        this.lat = lat;
        this.lon = lon;
        this.altitude = alt;
    }



    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getAltitude() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }



}
