package com.example.jonathan.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jonathan on 10.09.2014.
 */
public class DataLogger extends Activity{

    DatabaseHandler db;
    Geocoder geocoder;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        //Global stuff that's used multiple places
        db = new DatabaseHandler(this);
        geocoder = new Geocoder(this);
        intent = getIntent();

        //Get the name and append it to the hello text
        String name = intent.getExtras().getString("name");
        if(name.length() <= 1){name = "Lazy Butt!";}
        TextView nameField = (TextView) findViewById(R.id.NameOutput);
        nameField.append(" " + name);



        //Will try to convert the gps coordinates into an address
        try {
            List<Address> address = geocoder.getFromLocation(
                    intent.getExtras().getDouble("lat"),
                    intent.getExtras().getDouble("lon"),
                    1);

            //Appends the address to text view
            TextView addressField = (TextView) findViewById(R.id.LocationOutput);
            addressField.append(" " + address.get(0).getAddressLine(0));


        } catch (Exception e){
            Toast.makeText(this, "Network is Unavailable", Toast.LENGTH_LONG).show();
        }



        //Converts previous saved locations to addresses
        List<String> locationArray = new ArrayList<String>();
        for (UserLocation userLocation : db.getAllLocations(name)){

            try {
                List<Address> address = geocoder.getFromLocation(
                        userLocation.getLat(),
                        userLocation.getLon(),
                        1);
                locationArray.add(userLocation.getName() + " was at " + address.get(0).getAddressLine(0) );

            //In the event that it fail to convert the coordinates to an address, the coordinates get posted.
            } catch (Exception e){
                locationArray.add(userLocation.getName() + " was at Lat: " + userLocation.getLat() + " Lon: " + userLocation.getLon());
            }
        }

        //Access and set up the list view
        ListView listView = (ListView) findViewById(R.id.locationListView);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, locationArray);
        listView.setAdapter(arrayAdapter);


    }

    public void SaveData(View view) {

        //Creates a new UserLocation and saves it in the database
        UserLocation temp = new UserLocation(
                db.getLocationCount(),
                intent.getExtras().getString("name"),
                intent.getExtras().getDouble("lat"),
                intent.getExtras().getDouble("lon"),
                intent.getExtras().getDouble("alt"));

        db.addLocation(temp);
        Toast.makeText(this, "Data Saved, ID number:" + (db.getLocationCount()-1), Toast.LENGTH_SHORT).show();

        GoBack(view);
    }

    public void GoBack(View view) {
        this.finish();
    }
}
