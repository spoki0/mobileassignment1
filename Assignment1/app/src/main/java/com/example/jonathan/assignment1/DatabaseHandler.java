package com.example.jonathan.assignment1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jonathan on 12.09.2014.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // Database version
    private static final int DATABASE_VERSION = 1;

    // Database and table name
    private static final String DATABASE_NAME = "locationManager";
    private static final String TABLE_DATA = "data";

    // Data columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LON = "longitude";
    private static final String KEY_ALT = "altitude";


    private List<UserLocation> getAll(String query){
        List<UserLocation> locationList = new ArrayList<UserLocation>();
        SQLiteDatabase db = this.getReadableDatabase();

        try {
            Cursor cursor = db.rawQuery(query, null);

            if (cursor.moveToFirst()) {
                do {

                    UserLocation userLocation = new UserLocation(
                            Integer.parseInt(cursor.getString(0)),
                            (String) cursor.getString(1),
                            Double.parseDouble(cursor.getString(2)),
                            Double.parseDouble(cursor.getString(3)),
                            Double.parseDouble(cursor.getString(4))
                    );
                    locationList.add(userLocation);

                } while (cursor.moveToNext());
            }

        } catch (Exception e){
            Log.d("db", "Database didn't return any results.");
        }

        return locationList;

    }


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_DATA_TABLE = "CREATE TABLE " + TABLE_DATA + "("
                + KEY_ID  + " INTEGER PRIMARY KEY, "
                + KEY_NAME+ " TEXT, "
                + KEY_LAT + " REAL, "
                + KEY_LON + " REAL, "
                + KEY_ALT + " REAL)";

        db.execSQL(CREATE_DATA_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA);
        onCreate(db);
    }



    // CRUD OPERATIONS

    // Adding new contact
    public void addLocation(UserLocation userLocation) {
        SQLiteDatabase db = this.getWritableDatabase();

        //Pulling values from the class
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, userLocation.getName());
        values.put(KEY_LAT, userLocation.getLat());
        values.put(KEY_LON, userLocation.getLon());
        values.put(KEY_ALT, userLocation.getAltitude());

        //inserting the data into the database
        db.insert(TABLE_DATA, null, values);
        db.close();

    }

    // Getting single contact
    public UserLocation getLocation(int id) {

        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT * FROM " + TABLE_DATA
                     + " WHERE " + KEY_ID + " = " + id;

        Cursor cursor = db.rawQuery(query, null);

        if(cursor != null){
            cursor.moveToFirst();

            UserLocation userLocation = new UserLocation(
                    Integer.parseInt( cursor.getString(0) ),
                    (String) cursor.getString(1),
                    Double.parseDouble(cursor.getString(2)),
                    Double.parseDouble( cursor.getString(3) ),
                    Double.parseDouble( cursor.getString(4) )
            );
            return userLocation;

        } else {
            return new UserLocation(0, "NO USER FOUND", 0, 0, 0);
        }
    }

    // Getting All Contacts
    public List<UserLocation> getAllLocations() {
        String query = "SELECT * FROM " + TABLE_DATA;

        return getAll(query);
    }



    // Getting All Contacts with a given name;
    public List<UserLocation> getAllLocations(String name) {
        String query = "SELECT * FROM " + TABLE_DATA
                + " WHERE " + KEY_NAME + " = \"" + name + "\"";

        return getAll(query);
    }





    // Getting contacts Count
    public int getLocationCount() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery( ("SELECT COUNT(*) FROM " + TABLE_DATA), null);

        if (cursor != null){
            cursor.moveToFirst();
            return cursor.getInt(0);
        }

        return 0;
    }


    // Getting contacts Count
    public int getLocationCount(String name) {
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "SELECT COUNT(*) FROM " + TABLE_DATA
                     + " WHERE " + KEY_NAME + " = \"" + name + "\"";
        Cursor cursor = db.rawQuery(query, null);

        if (cursor != null){
            cursor.moveToFirst();
            return cursor.getInt(0);
        }

        return 0;
    }



    // Updating single contact
    public int updateLocation(UserLocation userLocation) {
        //TODO this
        return 0;
    }

    // Deleting single contact
    public void deleteLocation(UserLocation userLocation) {
        //TODO
    }

}
